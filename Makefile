.PHONY: test setup-virtualenv clone-repos

clone-repos:
	if [ ! -d "vengeance" ]; then \
		git clone git@bitbucket.org:atatsu/vengeance.git; \
	fi;
	if [ ! -d "vengeance-protocol-7dtd" ]; then \
		git clone git@@bitbucket.org:atatsu/vengeance-protocol-7dtd.git; \
	fi;
	if [ ! -d "vengeance-protocol-ark" ]; then \
		git clone git@bitbucket.org:atatsu/vengeance-protocol-ark.git; \
	fi;
	if [ ! -d "vengeance-protocol-sqlite3" ]; then \
		git clone git@bitbucket.org:atatsu/vengeance-protocol-sqlite3.git; \
	fi;
	if [ ! -d "vengeance-bot-7dtd-feralcountdown" ]; then \
		git clone git@bitbucket.org:atatsu/vengeance-bot-7dtd-feralcountdown.git; \
	fi;
	if [ ! -d "vengeance-bot-7dtd-hordewatch" ]; then \
		git clone git@bitbucket.org:atatsu/vengeance-bot-7dtd-hordewatch.git; \
	fi;
	if [ ! -d "vengeance-bot-7dtd-stats" ]; then \
		git clone git@bitbucket.org:atatsu/vengeance-bot-7dtd-stats.git; \
	fi;
	if [ ! -d "vengeance-bots-ark-tameutils" ]; then \
		git clone git@bitbucket.org:atatsu/vengeance-bots-ark-tameutils.git; \
	fi;
	if [ ! -d "vengeance-bot-ark-mastermind" ]; then \
		git clone git@bitbucket.org:atatsu/vengeance-bot-ark-mastermind.git; \
	fi;

setup-virtualenv:
	if [ ! -d "virtualenv" ]; then \
		virtualenv virtualenv; \
	fi;
	virtualenv/bin/pip install neovim;
	cd vengeance \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;
	cd vengeance-protocol-7dtd \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;
	cd vengeance-protocol-ark \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;
	cd vengeance-protocol-sqlite3 \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;
	cd vengeance-bot-7dtd-feralcountdown \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;
	cd vengeance-bot-7dtd-hordewatch \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;
	cd vengeance-bot-7dtd-stats \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;
	cd vengeance-bots-ark-tameutils \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;
	cd vengeance-bot-ark-mastermind \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop;

uninstall-dev-eggs:
	if [ ! -d "virtualenv" ]; then \
		echo "No virtualenv setup, how can there be dev eggs?"; \
	fi;
	cd vengeance \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	cd vengeance-protocol-7dtd \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	cd vengeance-protocol-ark \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	cd vengeance-protocol-sqlite3 \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	cd vengeance-bot-7dtd-feralcountdown \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	cd vengeance-bot-7dtd-hordewatch \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	cd vengeance-bot-7dtd-stats \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	cd vengeance-bots-ark-tameutils \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	cd vengeance-bot-ark-mastermind \
		&& ../virtualenv/bin/pip install -r requirements/test.txt \
		&& ../virtualenv/bin/python setup.py develop --uninstall;
	find . -name "*.egg-info" -type d -exec rm -r "{}" \;

test:
	cd vengeance && ../virtualenv/bin/python -m pytest -v;
	cd vengeance-protocol-7dtd && ../virtualenv/bin/python -m pytest -v;
	cd vengeance-protocol-ark && ../virtualenv/bin/python -m pytest -v;
	cd vengeance-protocol-sqlite3 && ../virtualenv/bin/python -m pytest -v;
	cd vengeance-bot-7dtd-feralcountdown && ../virtualenv/bin/python -m pytest -v;
	cd vengeance-bot-7dtd-hordewatch && ../virtualenv/bin/python -m pytest -v;
	cd vengeance-bot-7dtd-stats && ../virtualenv/bin/python -m pytest -v;
	cd vengeance-bots-ark-tameutils && ../virtualenv/bin/python -m pytest -v;
	cd vengeance-bot-ark-mastermind && ../virtualenv/bin/python -m pytest -v;
